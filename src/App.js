import React, {useEffect} from 'react';
import './App.css';

import Login from './pages/LoginPage';
import LoadedData from './pages/LoadedData';

function App() {

const object = [
  {
    title: 'MGS1',
    text: 'Solid Snake',
    quantity: 1
  },
  {
    title: 'MGS2',
    text: 'Solidus Snake',
    quantity: 1
  },
  {
    title: 'MGS3',
    text: 'Naked Snake',
    quantity: 1
  }
]

useEffect(() => {
  localStorage.getItem('email');
  localStorage.getItem('password');
  console.log(localStorage)
}, [localStorage])

function logOut(){
  localStorage.clear();
  alert('Logged out')
}

  return (
    <>
      <div style={{background: "#303841", color: '#F9AE58', display: 'flex', justifyContent: 'space-evenly'}}>
        {
          localStorage.getItem('email') === null || localStorage.getItem('password') === null
          ?
          <>
            <label htmlFor="">Navbar</label>
            <label htmlFor="">Home</label>
            <label htmlFor="">Login</label>
            <label htmlFor="">Register</label>
          </>
          :
          <>
            <label htmlFor="">Navbar</label>
            <label htmlFor="">Home</label>
            <label htmlFor="" onClick={logOut}>Log out</label>
          </>
        }
      </div>
      <Login />
      <LoadedData LoadedData={object}/>
    </>
  );
}

export default App;
