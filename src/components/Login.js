import React from 'react';

export default function Login(props){
  return(
      <>
        Log in:
        <div>
          <input type="email" placeholder="enter email" 
          onChange={props.email}/>
          <h1>Name: {props.h1}</h1>
        </div>

        <div>
          <input type="password" placeholder="enter password" onChange={props.password} />
        </div>

        <div>
          <button onClick={props.btn}>Log in</button>
        </div>
      </>
    )
  }