import React from 'react';

export default function Data(props){
	const {title, text, quantity} = props;
	return(
			<>
				<div>
					<div>
						Title: {title}
					</div>
					<div>
						Text: {text}
					</div>
					<div>
						Quantity: {quantity}
					</div>
				</div>
			</>
		)
}