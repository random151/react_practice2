import React from 'react';
import Data from '../components/Data';

export default function LoadedData(props){
	const loadedData = props.LoadedData.map((data) => {
		return(
				<Data title={data.title} text={data.text} quantity={data.quantity}/>
			)
	});

	return(
			<>
				{loadedData}
			</>
		)
}